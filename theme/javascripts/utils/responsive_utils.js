var ResponsiveUtils = (function() {
  'use strict';

  var breakpoints = {
    small  : 767,
    medium : 992,
    large  : 1200
  };

  var responsiveUtils = {};

  responsiveUtils.respondTo = function(bp, on, off) {
    if (window.matchMedia('(min-width: ' + breakpoints[bp] + 'px)').matches) {
      return on();
    } else if (off) {
      return off();
    }
  }

  responsiveUtils.breakpoints = breakpoints;

  return responsiveUtils;
})();