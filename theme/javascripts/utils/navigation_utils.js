var NavigationUtils = (function() {
  'use strict';

  var navigationUtils = {};

  // scroll animation duration in milliseconds
  var SCROLL_ANIMATION_DURATION = 1000;

  /**
   * Scrolls the page to a specific section
   *
   * Example usage:
   * $('.my-navigation-button').on('click', function() {
   *   NavigationUtils.navigateToSection('#my-section');
   * });
   *
   **/
  navigationUtils.navigateToSection = function(section, offset = 0) {
    $('html, body').animate({
      scrollTop: $(section).offset().top - offset
    }, SCROLL_ANIMATION_DURATION);
  }

  return navigationUtils;
})();
