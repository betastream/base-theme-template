// Version: 1.0
// Date: 2016/06/23

var gulp = require('gulp');
var rev = require('gulp-rev');
var revReplace = require('gulp-rev-replace');
var revNapkin = require('gulp-rev-napkin');
var useref = require('gulp-useref');
var sass = require('gulp-sass');
var csso = require('gulp-csso');
var autoprefixer = require('gulp-autoprefixer');
var gulpConcat = require('gulp-concat');
var babel = require('gulp-babel');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var rsync = require('gulp-rsync');
var del = require('del');
var runSequence = require('run-sequence');
var fs = require('fs');
var merge = require('merge');

// load configuration
var config = require('./config.json');

// var bowerConfig = require('./' + themePath + 'bower.json');
// bowerConfig.dependencies.map(function(d) { console.log(d); });
// console.log(bowerConfig);

var syncTasks = [];
if (config.project.middleman.path.length > 0) {
  syncTasks.push('sync_mm');
}
if (config.project.wordpress.path.length > 0) {
  syncTasks.push('sync_wp');
}

var themePath = config.project.theme.path + '/';
var wpThemesFolder = config.project.wordpress.path + '/__www/wp-content/themes/';
var wpMainFolder = wpThemesFolder + config.project.slug + '/';
var mmMainFolder = config.project.middleman.path + '/source/';
var acfFolder = config.project.theme.acf_path;
var paths = {
  main:    themePath,
  js:      themePath + 'javascripts/',
  jsLibs:  themePath + 'bower_components/',
  sass:    themePath + 'stylesheets/',
  fonts:   themePath + 'fonts/',
  tmp:     themePath + 'tmp/',
  assets:  themePath + 'assets/',
  layouts: themePath + 'layouts/',
  acf_fields: themePath + acfFolder + '/',
  wpTheme:  wpMainFolder,
  wpFonts:   wpMainFolder + 'fonts/',
  wpLayouts: wpMainFolder + 'layouts/',
  mmMain: mmMainFolder
}

var jsLibFiles = config.project.javascripts.libs.map(function(f,i){ return paths.jsLibs + f});
var jsCustomFiles = config.project.javascripts.custom.map(function(f,i){ return paths.js + f});
// files you don't want concatenated or otherwise treated
var unchangeableJsLibFiles = config.project.javascripts.unLibs.map(function(f,i){ return paths.jsLibs + f});
// files you don't want concatenated or otherwise treated
var unchangeableJsCustomFiles = config.project.javascripts.unCustom.map(function(f,i){ return paths.js + f});

var jsFiles = jsLibFiles.concat(jsCustomFiles);
var unchangeableJsFiles = unchangeableJsLibFiles.concat(unchangeableJsCustomFiles);

var options = {
  sass: {}
}

function assertFilesExist(files) {
  files.forEach(function(f) { fs.accessSync(f, fs.F_OK) });
}

gulp.task('watch', ['dev'], function () {
  gulp.watch(paths.sass + '**/*.scss', ['sass:build_and_sync']);
  gulp.watch(paths.js   + '**/*.js',   ['js:build_and_sync']);
  gulp.watch([paths.main + '**/*.twig', '!' + paths.tmp + '**/*.twig'],  ['sync']);
  gulp.watch([paths.main + '**/*.php'], ['sync']);
  gulp.watch([paths.acf_fields + '**/*.json'], ['sync']);
});

gulp.task('clean', function() {
  del([paths.tmp, paths.assets + '/*']);
});

gulp.task('sass:build', function () {
  return gulp.src(paths.sass + 'all.scss')
    .pipe(rename('all.css'))
    .pipe(sourcemaps.init())
      .pipe(sass(options.sass).on('error', sass.logError))
      .pipe(autoprefixer({ browsers: ['last 2 versions', 'Explorer >= 8'] }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.assets));
});

gulp.task('sass:build_and_sync', function() {
  runSequence('sass:build', 'sync');
});

gulp.task('css:minify', ['sass:build'], function () {
  return gulp.src(paths.assets + 'all.css')
    .pipe(csso())
    .pipe(gulp.dest(paths.assets));
});

gulp.task('js:copy_unchangeable', function () {
  return gulp.src(unchangeableJsFiles)
    .pipe(gulp.dest(paths.assets));
});

gulp.task('js:build', ['js:copy_unchangeable'], function () {
  assertFilesExist(jsFiles);
  return gulp.src(jsFiles)
    .pipe(sourcemaps.init())
      .pipe(babel({ presets: ['es2015'] }))
      .pipe(gulpConcat('all.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.assets));
});

gulp.task('js:build_and_sync', ['js:build'], function(){
  runSequence('js:build', 'sync');
});

gulp.task('js:minify', ['js:build'], function () {
  return gulp.src(paths.assets + 'all.js')
    .pipe(uglify())
    .pipe(gulp.dest(paths.assets));
});

gulp.task('rev-assets', ['css:minify', 'js:minify'],  function() {
  return gulp.src(paths.assets + '*')
    .pipe(rev()) // Add hash
    .pipe(gulp.dest(paths.assets))
    .pipe(revNapkin({verbose: false}))
    .pipe(rev.manifest('rev-manifest.json', { merge: true }))
    .pipe(gulp.dest(paths.assets));
});

gulp.task('update_html',  function() {
  var manifest = gulp.src(paths.assets + 'rev-manifest.json');

  return gulp.src(paths.layouts + 'layout.twig')
    .pipe(revReplace({ // Substitute in new filenames
      manifest: manifest,
      replaceInExtensions: ['.twig']
    }))
    .pipe(gulp.dest(paths.tmp));
});

var commonSyncExclusions = [
  'javascripts',
  'stylesheets',
  'tmp',
  'bower_components'
];

var commonSyncSettings = {
  silent: true,
  root: paths.main,
  recursive: true,
  clean: true
}

gulp.task('sync_mm', function() {
  return gulp.src(paths.main)
    .pipe(rsync(Object.assign({}, commonSyncSettings, {
      destination: paths.mmMain,
      exclude: commonSyncExclusions.concat(['**/*.php', acfFolder ])
    })));
});

gulp.task('sync_wp', function() {
  return gulp.src(paths.main)
    .pipe(rsync(Object.assign({}, commonSyncSettings, {
      destination: paths.wpTheme,
      exclude: commonSyncExclusions.concat(['**/*.erb'])
    })));
});

gulp.task('sync', syncTasks);

gulp.task('copy_layout', function() {
  return gulp.src(paths.tmp + 'layout.twig')
    .pipe(rsync({
      root: paths.tmp,
      destination: paths.wpLayouts
    }));
});

gulp.task('dev', function() {
  options.sass = { sourceComments: true }
  runSequence('clean', ['sass:build', 'js:build'], 'sync');
});

gulp.task('prod', function() {
  runSequence('clean', 'rev-assets', 'update_html', 'sync', 'copy_layout');
});

gulp.task('default',['watch']);
