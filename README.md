##<project-name> Theme project##

The project builds a site using [Twig](http://twig.sensiolabs.org/documentation) templates. The source can be used by Middleman and/or as a Wordpress theme.
To achieve this, the code is developed under the `theme` folder. The resulting assets are then built and copied to the `middleman` and/or `wordpress` folders.

*NEVER work on the site assets under the `middleman` or `wordpress`s folders or your work may be lost.*

    .
    +- theme
    |  . (develop theme assets. This is the working folder)
    |  +- assets (built assets. Don't add this folder to git)
    |  +- bower_components (bower assets. Don't add this folder to git)
    +- middleman
    |  +- source
    |     +- (copied theme assets. Don't add this folder to git)
    +- wordpress
    |  ...
    |  +- wp-content
    |     ...
    |     +- themes
    |        ...
    |        +- <project-slug>
    |           ...
    |           +- (copied theme assets. Don't add this folder to git)
    +- node_modules (npm assets. Don't add this folder to git)

To work on the project you must install the following applications:
  * `ruby`
  * `node` and `npm`

On the bin folder you will find some useful scripts. To install all required packages run `$ bin/install`. This will fetch and install the `npm`, `bower` and `bundle` packages.

 * `npm` - node packages used by the gulp command
 * `bower` - javascript packages used by site
 * `bundle` - ruby packages used by middleman

### Build project assets ###

To build the project assets use the `gulp` command.

`$ gulp watch` or just `$ gulp` will compile all the site assets with development information and copy them to the working folders. Then it will start to monitor the development folder for changes. If any file changes, it will build and copy the altered assets.

`$ gulp dev` will build the assets with development information (no compression) and then copy them.

`$ gulp prod` will build a minified version of the assets and then copy them.

### Middleman ###

To develop with middleman you need to run the Middleman server.

*Make sure you build the assets with the `gulp` command before you start the Middleman server*

To start middleman server, from the middleman folder, run `$ bundle exec middleman`
Then you can open http://localhost:/4567 on your browser.

### Wordpress ###

On the `wordpress\__wp_config` folder, copy the sample configuration file `env-sample.php` to `env.php`.
Edit the file and change the `DB_HOST`, `DB_NAME`, `DB_PASSWORD` and `DB_USER` variables to point to your database.

Restore a database dump.

#### Configuring Wordpress for the first time ####

If you don't have a database dump you will need to create a new one.

...

#### Adding Wordpress plugins ####

You can add Wordpress plugins using `composer` modules. Almost any public available plugin can be installed as a `composer` module. Please check https://wpackagist.org for more information.

Add the plugin to `composer.json` under `"require"`
    ...
    "require": {
        ...
        "<name-space>/<plugin-name>": "<version>",
    ...

* Custom Plugins - Under project root theme you should have (or may create) a directory called ´wordpress-custom-plugins´. Inside that directory add the plugin you are developing. Don't forget To add `{
      "type": "path",
      "url": "./wordpress-custom-plugins/your-custom-plugin",
      "options": {
          "symlink": false
      }
    },` to `composer.json` file.
    If you are still developing the plugin set the `symlink` option to `true`. Otherwise set it to `false`.

* Git submodule - Some plugins are not public available or need to be included as submodule.

Always use a specific version.

Team modules are served by a Satis server https://page-io-satis.herokuapp.com. You can add them to your `composer.json` as any other module.

Sometimes we need to develop specific plugins that will be used only on the project. We can do that using a special setup on composer.

Put your plugin code under the `<project-root>/wordpress-custom-plugins/<plugin-name>`.
Add a minimal composer file:

    {
        "name": "custom-plugin/<plugin-name>",
        "version": "0.1",
        "type": "wordpress-plugin"
    }

Add the folder as `repository` to `composer.json` and include it as a required plugin.

    ...
    "repositories": [
        ...
        {
            "type": "path",
            "url": "./wordpress-custom-plugins/<plugin-name>",
            "options": {
                "symlink": false
            }
        },
    ...
    "require": {
        ...
        "custom-plugin/<plugin-name>": "*",
    ...

During development, you may wish to set the `symlink` option to true. This will make the a symlink between the installed plugin folder on Wordpress and your development folder. This way, your changes will take effect when you save them allowing for a faster development process.

**Never commit this option to true or it will break the build.**

**Never add your plugin directly to the Wordpress plugins folder or it will be deleted.**

Each time you change the `composer.json` file, you need to run the `composer update` command.
This command will create or update the `composer.lock` file. This file needs to be commit to git.
When different developers change the `composer.json` at the same time in different branches, a merge conflict will occur when we try to merge them together. Those conflicts are easy to solve since `composer.lock` is a generated file. Just reset the `composer.json` file (don't delete it) and run `composer update` again. Commit the resulting `composer.lock` file.

Each time you pull changes from other developers that changed the `composer.json` file, you will need to run the `composer install` command to update your Wordpress.

#### Adding Wordpress Custom Fields ####

By default we install the ACF-Pro plugin for custom fields in Wordpress. This is what allows us to define custom data to associate with Wordpress defined structures, such as posts or pages.
Traditionally we would create them in the admin area on the Custom Fields section. However, this means that on deploys someone would have to go on the backoffice and recreate any changes to the custom fields. Which is why we use the plugin's local JSON feature to synchronize them automatically, instead of having to do it manually.
For more info on this, see [Local JSON](https://www.advancedcustomfields.com/resources/local-json/) and [Synchronized JSON](https://www.advancedcustomfields.com/resources/synchronized-json/).

To generate the JSON file with definitions for a given Custom Field Group in the Wordpress admin area:

1. Go to **Custom Fields** > **Tools**

2. Select the Custom Field Group you wish to export

3. Press ""

4. Edit the generated JSON file and remove the leading `[` and trailing `]`

After that, simply add the file to the `acf_path` configured directory (by default `acf-json`, see `config.json`) under the `theme` directory.

The ACF-Pro plugin is payed and you will require a key to install it.
On the root of the project, add a file named `.env` with the key.

`ACF_KEY=<actual key>`

### Guide lines ###

To ensure code quality and consistency please refer to the [Style Guide](https://betastream.atlassian.net/wiki/pages/viewpage.action?pageId=19595379).

Use [gitflow](https://betastream.atlassian.net/wiki/display/KB/Gitflow+approach+to+development).
Development is done in features branches with Pull Requests to merge to develop.
Always prefix your commit message with the JIRA card id.
Wait for Pull Request approval before merging to develop branch (the PR is merged by the developer).

#### How to add a new node module ####

##### Using Shrinkpack (the prefered way) #####

First, you'll need `shrinkpack` installed, I recommend you do so by running `npm install -g shrinkpack`.

Install the node module by running the install command:

`$ npm install <module-name> --save`

The command will install the scripts under `theme/node_modules` folder.

Now for shrinkpack, run the following commands:

* (*Optional, for optimization*) `$ npm prune` followed by `$ npm dedupe`
* `$ npm shrinkwrap --dev`
* `$ shrinkpack`

Tis will create the `theme/node_shrinkrwap` folder and add tarballs for the required node modules in it.
This way, when another dev, or a server build, run `npm install` it will use these instead of having to download them from the npm registry.

**Note:** Never add the `node_shrinkwrap` folder to .gitignore, it is essential that its contents are in the repo for this process.

For further info on what shrinkpack is, does, and why we use it, go [here](https://github.com/JamieMason/shrinkpack).

##### Updating existing modules or other changes to `package.json` #####

Any changes to the node modules should be followed by running:

* (*Optional, for optimization*) `$ npm prune` followed by `$ npm dedupe`
* `$ npm shrinkwrap --dev`
* `$ shrinkpack`

#### How to add a new javascript library ####

##### Using Bower (the prefered way) #####

Install the bower package by running the install command:

`$ bower install <package-name> --save-dev`

The command will install the scripts under `theme/bower_components` folder.
Add the main file to the `config.json` file in `project.javascripts.libs`.

##### Adding to vendor #####

...